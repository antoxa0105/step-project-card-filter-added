import { templates } from "../data/templates.js";

export class LogInForm {
  setUserData () {
      const userData = {
       emailInput: document.querySelector(`input[name="email"]`),
       passwordInput: document.querySelector(`input[name="password"]`),
      }
      return userData
  }

  openModal () { 
    const modal = document.createElement('div')
    modal.classList.add('modal', 'show')
    modal.setAttribute('id', 'myModal');
    modal.setAttribute('tabindex', '-1')
    modal.innerHTML = templates.loginForm;
    document.body.appendChild(modal)
    this.myModal = new bootstrap.Modal(document.getElementById('myModal'), {})
    this.myModal.show();
   }
   
  closeModal() {
      this.myModal.dispose()
      myModal.remove()
  }    
}

