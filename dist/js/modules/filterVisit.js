"use strict";

import { Modal } from "./Modal.js";
import { Visit } from "./Visit/Visit.js";
import { templates } from "../data/templates.js";

export function filterVisit(e, itemForFilter, data = [], procedure = () => {console.warn("PROCEDURE")}) { 
  console.log(itemForFilter);
    const filteredList = data.filter((element) => {  
      const doctor = element[itemForFilter];
      return doctor.includes(e.value); 
    });
    document.querySelectorAll(".visit").forEach(element => element.remove())
    const modal = new Modal;
      filteredList.forEach((visit) => 
      new Visit(
        templates.visit[visit.doctor.toLowerCase()],
        visit).render("desk"));
        procedure();
}

